EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "I2C Multiplexer RJ45 Hub"
Date ""
Rev "v0.1.1"
Comp "Universität Tübingen"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CO2Mofetten:RJ45SensorCableConnector J5
U 1 1 5E30283D
P 4900 7050
F 0 "J5" V 5003 6620 50  0000 R CNN
F 1 "RJ45SensorCableConnector" V 4912 6620 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 4900 7075 50  0001 C CNN
F 3 "~" V 4900 7075 50  0001 C CNN
	1    4900 7050
	0    -1   -1   0   
$EndComp
$Comp
L CO2Mofetten:Adafruit_TCA9548A U1
U 1 1 5E307AB8
P 4850 4950
F 0 "U1" H 4850 5665 50  0000 C CNN
F 1 "Adafruit_TCA9548A" H 4850 5574 50  0000 C CNN
F 2 "CO2Mofetten:Adafruit_TCA9548A_PinSocket" H 4850 5550 50  0001 C CNN
F 3 "https://cdn-shop.adafruit.com/datasheets/tca9548a.pdf" H 4850 5550 50  0001 C CNN
F 4 "https://learn.adafruit.com/adafruit-tca9548a-1-to-8-i2c-multiplexer-breakout/overview" H 4900 4050 50  0001 C CNN "URL"
	1    4850 4950
	1    0    0    -1  
$EndComp
$Comp
L CO2Mofetten:RJ45SensorCableConnector J2
U 1 1 5E30B9F9
P 2400 4350
F 0 "J2" V 2503 3920 50  0000 R CNN
F 1 "RJ45SensorCableConnector" V 2412 3920 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 2400 4375 50  0001 C CNN
F 3 "~" V 2400 4375 50  0001 C CNN
	1    2400 4350
	1    0    0    -1  
$EndComp
$Comp
L CO2Mofetten:RJ45SensorCableConnector J1
U 1 1 5E30C5B4
P 2400 2900
F 0 "J1" V 2503 2470 50  0000 R CNN
F 1 "RJ45SensorCableConnector" V 2412 2470 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 2400 2925 50  0001 C CNN
F 3 "~" V 2400 2925 50  0001 C CNN
	1    2400 2900
	1    0    0    -1  
$EndComp
$Comp
L CO2Mofetten:RJ45SensorCableConnector J4
U 1 1 5E30DD60
P 4050 1400
F 0 "J4" V 4153 970 50  0000 R CNN
F 1 "RJ45SensorCableConnector" V 4062 970 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 4050 1425 50  0001 C CNN
F 3 "~" V 4050 1425 50  0001 C CNN
	1    4050 1400
	0    1    1    0   
$EndComp
$Comp
L CO2Mofetten:RJ45SensorCableConnector J6
U 1 1 5E30DEE5
P 5550 1400
F 0 "J6" V 5653 970 50  0000 R CNN
F 1 "RJ45SensorCableConnector" V 5562 970 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 5550 1425 50  0001 C CNN
F 3 "~" V 5550 1425 50  0001 C CNN
	1    5550 1400
	0    1    1    0   
$EndComp
$Comp
L CO2Mofetten:RJ45SensorCableConnector J7
U 1 1 5E30EC9F
P 7150 2800
F 0 "J7" V 7253 2370 50  0000 R CNN
F 1 "RJ45SensorCableConnector" V 7162 2370 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 7150 2825 50  0001 C CNN
F 3 "~" V 7150 2825 50  0001 C CNN
	1    7150 2800
	-1   0    0    1   
$EndComp
$Comp
L CO2Mofetten:RJ45SensorCableConnector J8
U 1 1 5E30F123
P 7200 4250
F 0 "J8" V 7303 3820 50  0000 R CNN
F 1 "RJ45SensorCableConnector" V 7212 3820 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 7200 4275 50  0001 C CNN
F 3 "~" V 7200 4275 50  0001 C CNN
	1    7200 4250
	-1   0    0    1   
$EndComp
$Comp
L CO2Mofetten:RJ45SensorCableConnector J9
U 1 1 5E30FC5D
P 7200 5600
F 0 "J9" V 7303 5170 50  0000 R CNN
F 1 "RJ45SensorCableConnector" V 7212 5170 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 7200 5625 50  0001 C CNN
F 3 "~" V 7200 5625 50  0001 C CNN
	1    7200 5600
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 6650 4550 6650
Wire Wire Line
	4600 6650 4700 6650
Connection ~ 4600 6650
Wire Wire Line
	4700 6650 4700 6600
Wire Wire Line
	4700 6600 4900 6600
Wire Wire Line
	4900 6600 4900 6650
Connection ~ 4700 6650
Wire Wire Line
	2800 5300 2800 5350
Wire Wire Line
	2800 5500 2850 5500
Wire Wire Line
	2850 5500 2850 5700
Wire Wire Line
	2850 5700 2800 5700
Wire Wire Line
	2800 3950 2800 4000
Wire Wire Line
	2800 4050 2800 4150
Connection ~ 2800 4050
Wire Wire Line
	2800 4150 2850 4150
Wire Wire Line
	2850 4150 2850 4350
Wire Wire Line
	2850 4350 2800 4350
Connection ~ 2800 4150
Wire Wire Line
	2800 2500 2800 2550
Wire Wire Line
	2800 2700 2850 2700
Wire Wire Line
	2850 2700 2850 2900
Wire Wire Line
	2850 2900 2800 2900
Wire Wire Line
	2800 2600 2800 2700
Connection ~ 2800 2600
Connection ~ 2800 2700
Wire Wire Line
	4450 1800 4400 1800
Wire Wire Line
	4350 1800 4250 1800
Connection ~ 4350 1800
Wire Wire Line
	4250 1800 4250 1850
Wire Wire Line
	4250 1850 4050 1850
Wire Wire Line
	4050 1850 4050 1800
Connection ~ 4250 1800
Wire Wire Line
	5950 1800 5900 1800
Wire Wire Line
	5750 1800 5850 1800
Connection ~ 5850 1800
Wire Wire Line
	5750 1800 5750 1850
Wire Wire Line
	5750 1850 5550 1850
Wire Wire Line
	5550 1850 5550 1800
Connection ~ 5750 1800
Wire Wire Line
	6750 3000 6750 3100
Connection ~ 6750 3100
Wire Wire Line
	6750 3100 6750 3150
Wire Wire Line
	6750 3000 6700 3000
Wire Wire Line
	6700 3000 6700 2800
Wire Wire Line
	6700 2800 6750 2800
Connection ~ 6750 3000
Wire Wire Line
	6800 4550 6800 4450
Connection ~ 6800 4550
Wire Wire Line
	6800 4450 6750 4450
Wire Wire Line
	6750 4450 6750 4250
Wire Wire Line
	6750 4250 6800 4250
Connection ~ 6800 4450
Wire Wire Line
	6800 6000 6800 5950
Wire Wire Line
	6800 5900 6800 5800
Connection ~ 6800 5900
Wire Wire Line
	6800 5800 6750 5800
Wire Wire Line
	6750 5800 6750 5600
Wire Wire Line
	6750 5600 6800 5600
Connection ~ 6800 5800
Wire Wire Line
	4550 6650 4550 6550
Wire Wire Line
	4250 6450 4250 4600
Wire Wire Line
	4250 4600 4450 4600
$Comp
L power:GND #PWR0101
U 1 1 5E35FFE3
P 3700 7300
F 0 "#PWR0101" H 3700 7050 50  0001 C CNN
F 1 "GND" H 3705 7127 50  0000 C CNN
F 2 "" H 3700 7300 50  0001 C CNN
F 3 "" H 3700 7300 50  0001 C CNN
	1    3700 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 7300 3700 6450
Wire Wire Line
	3700 6450 4250 6450
Connection ~ 4250 6450
$Comp
L power:+5V #PWR0102
U 1 1 5E36269A
P 6450 900
F 0 "#PWR0102" H 6450 750 50  0001 C CNN
F 1 "+5V" H 6465 1073 50  0000 C CNN
F 2 "" H 6450 900 50  0001 C CNN
F 3 "" H 6450 900 50  0001 C CNN
	1    6450 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 6500 5200 6650
Connection ~ 4550 6650
Wire Wire Line
	4550 6650 4600 6650
Wire Wire Line
	4250 6450 4550 6450
Wire Wire Line
	5000 6650 5000 6000
Wire Wire Line
	5000 6000 4350 6000
Wire Wire Line
	4350 6000 4350 4700
Wire Wire Line
	4350 4700 4450 4700
Wire Wire Line
	4800 6650 4800 5850
Wire Wire Line
	4800 5850 4400 5850
Wire Wire Line
	4400 5850 4400 4800
Wire Wire Line
	4400 4800 4450 4800
Wire Wire Line
	4450 5300 3050 5300
Wire Wire Line
	3050 5300 3050 5800
Wire Wire Line
	3050 5800 2800 5800
Wire Wire Line
	2950 5400 2950 5600
Wire Wire Line
	2950 5600 2800 5600
Wire Wire Line
	4450 5500 3450 5500
Wire Wire Line
	3450 5500 3450 4450
Wire Wire Line
	3450 4450 2800 4450
Wire Wire Line
	3150 5600 3150 4250
Wire Wire Line
	3150 4250 2800 4250
Wire Wire Line
	5250 4500 5350 4500
Wire Wire Line
	5350 3650 3000 3650
Wire Wire Line
	3000 3650 3000 2800
Wire Wire Line
	3000 2800 2800 2800
Wire Wire Line
	5250 4600 5300 4600
Wire Wire Line
	5300 4600 5300 3550
Wire Wire Line
	5300 3550 2900 3550
Wire Wire Line
	2900 3550 2900 3000
Wire Wire Line
	2900 3000 2800 3000
Wire Wire Line
	5450 4700 5450 2850
Wire Wire Line
	5450 2850 4150 2850
Wire Wire Line
	4150 2850 4150 1800
Wire Wire Line
	5250 4800 5550 4800
Wire Wire Line
	5550 4800 5550 2600
Wire Wire Line
	5550 2600 3950 2600
Wire Wire Line
	3950 2600 3950 1800
Wire Wire Line
	5650 4900 5650 1800
Wire Wire Line
	5450 1800 5450 2300
Wire Wire Line
	5450 2300 5750 2300
Wire Wire Line
	5750 2300 5750 5000
Wire Wire Line
	5750 5000 5250 5000
Wire Wire Line
	6000 5100 6000 2900
Wire Wire Line
	6000 2900 6750 2900
Wire Wire Line
	6750 2700 6100 2700
Wire Wire Line
	6100 2700 6100 5200
Wire Wire Line
	6100 5200 5250 5200
Wire Wire Line
	6400 4350 6800 4350
Wire Wire Line
	6800 4150 6200 4150
Wire Wire Line
	6200 4150 6200 5400
Wire Wire Line
	6200 5400 5250 5400
Wire Wire Line
	6500 5500 6500 5700
Wire Wire Line
	6500 5700 6800 5700
Wire Wire Line
	6800 5500 6700 5500
Wire Wire Line
	6700 5600 5250 5600
Wire Wire Line
	6450 900  6450 2050
Wire Wire Line
	5200 6500 6450 6500
Wire Wire Line
	6800 3950 6450 3950
Connection ~ 6450 3950
Connection ~ 6450 2050
Wire Wire Line
	3700 6450 3700 5350
Wire Wire Line
	3700 2400 4400 2400
Wire Wire Line
	6350 2400 6350 3150
Connection ~ 3700 6450
Connection ~ 6800 5950
Wire Wire Line
	6800 5950 6800 5900
Wire Wire Line
	6350 3150 6650 3150
Connection ~ 6350 3150
Wire Wire Line
	6350 3150 6350 4600
Connection ~ 6750 3150
Wire Wire Line
	6750 3150 6750 3200
Wire Wire Line
	5900 1800 5900 1950
Connection ~ 5900 1800
Wire Wire Line
	5900 1800 5850 1800
Connection ~ 5900 2400
Wire Wire Line
	5900 2400 6350 2400
Wire Wire Line
	4400 1800 4400 1900
Connection ~ 4400 1800
Wire Wire Line
	4400 1800 4350 1800
Connection ~ 4400 2400
Wire Wire Line
	4400 2400 5900 2400
Wire Wire Line
	2800 5350 2900 5350
Connection ~ 2800 5350
Connection ~ 3700 5350
Wire Wire Line
	3700 5350 3700 4000
Wire Wire Line
	2800 4000 2950 4000
Connection ~ 2800 4000
Wire Wire Line
	2800 4000 2800 4050
Connection ~ 3700 4000
Wire Wire Line
	3700 4000 3700 2550
Wire Wire Line
	2800 2550 3300 2550
Connection ~ 2800 2550
Wire Wire Line
	2800 2550 2800 2600
Connection ~ 3700 2550
Wire Wire Line
	3700 2550 3700 2400
Connection ~ 6350 4600
Wire Wire Line
	6350 4600 6350 5950
Wire Wire Line
	6800 4550 6800 4600
Connection ~ 6800 4600
Wire Wire Line
	6800 4600 6800 4650
Wire Wire Line
	6400 4350 6400 5300
Wire Wire Line
	6350 4600 6700 4600
Wire Wire Line
	2800 5350 2800 5400
Wire Wire Line
	2800 5400 2800 5500
Connection ~ 2800 5400
Connection ~ 2800 5500
$Comp
L CO2Mofetten:RJ45SensorCableConnector J3
U 1 1 5E30B322
P 2400 5700
F 0 "J3" V 2503 5270 50  0000 R CNN
F 1 "RJ45SensorCableConnector" V 2412 5270 50  0000 R CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 2400 5725 50  0001 C CNN
F 3 "~" V 2400 5725 50  0001 C CNN
	1    2400 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 5500 6500 5500
Wire Wire Line
	5250 5300 6400 5300
Wire Wire Line
	5250 5100 6000 5100
Wire Wire Line
	5250 4900 5650 4900
Wire Wire Line
	5250 4700 5450 4700
Wire Wire Line
	5350 3650 5350 4500
Wire Wire Line
	2950 5400 4450 5400
Wire Wire Line
	3150 5600 4450 5600
Wire Wire Line
	7000 5100 7100 5100
Wire Wire Line
	7100 5100 7300 5100
Connection ~ 7100 5100
Connection ~ 7300 5100
Wire Wire Line
	7300 5100 7400 5100
Wire Wire Line
	5400 6850 5400 6950
Connection ~ 5400 6950
Wire Wire Line
	5400 6950 5400 7150
Connection ~ 5400 7150
Wire Wire Line
	5400 7150 5400 7250
Wire Wire Line
	5400 6850 5400 6550
Wire Wire Line
	5400 6550 4550 6550
Connection ~ 5400 6850
Connection ~ 4550 6550
Wire Wire Line
	4550 6550 4550 6450
Wire Wire Line
	6350 5950 6600 5950
Wire Wire Line
	6700 5500 6700 5600
Wire Wire Line
	7000 5100 6600 5100
Wire Wire Line
	6600 5100 6600 5950
Connection ~ 7000 5100
Connection ~ 6600 5950
Wire Wire Line
	6600 5950 6800 5950
Wire Wire Line
	7400 3750 7300 3750
Wire Wire Line
	6700 3750 6700 4600
Connection ~ 7000 3750
Wire Wire Line
	7000 3750 6700 3750
Connection ~ 7100 3750
Wire Wire Line
	7100 3750 7000 3750
Connection ~ 7300 3750
Wire Wire Line
	7300 3750 7100 3750
Connection ~ 6700 4600
Wire Wire Line
	6700 4600 6800 4600
Wire Wire Line
	7350 2300 7250 2300
Connection ~ 7050 2300
Wire Wire Line
	7050 2300 6950 2300
Connection ~ 7250 2300
Wire Wire Line
	7250 2300 7050 2300
Wire Wire Line
	6950 2300 6650 2300
Wire Wire Line
	6650 2300 6650 3150
Connection ~ 6950 2300
Connection ~ 6650 3150
Wire Wire Line
	6650 3150 6750 3150
Wire Wire Line
	5050 1200 5050 1300
Connection ~ 5050 1300
Wire Wire Line
	5050 1300 5050 1500
Connection ~ 5050 1500
Wire Wire Line
	5050 1500 5050 1600
Wire Wire Line
	5050 1600 5050 1950
Wire Wire Line
	5050 1950 5900 1950
Connection ~ 5050 1600
Connection ~ 5900 1950
Wire Wire Line
	5900 1950 5900 2400
Wire Wire Line
	3550 1200 3550 1300
Wire Wire Line
	3550 1900 4400 1900
Connection ~ 3550 1300
Wire Wire Line
	3550 1300 3550 1500
Connection ~ 3550 1500
Wire Wire Line
	3550 1500 3550 1600
Connection ~ 3550 1600
Wire Wire Line
	3550 1600 3550 1900
Connection ~ 4400 1900
Wire Wire Line
	4400 1900 4400 2400
Wire Wire Line
	2200 3400 2300 3400
Wire Wire Line
	3300 3400 3300 2550
Connection ~ 2300 3400
Wire Wire Line
	2300 3400 2500 3400
Connection ~ 2500 3400
Wire Wire Line
	2500 3400 2600 3400
Connection ~ 2600 3400
Wire Wire Line
	2600 3400 3300 3400
Connection ~ 3300 2550
Wire Wire Line
	3300 2550 3700 2550
Wire Wire Line
	2200 4850 2300 4850
Wire Wire Line
	2950 4850 2950 4000
Connection ~ 2300 4850
Wire Wire Line
	2300 4850 2500 4850
Connection ~ 2500 4850
Wire Wire Line
	2500 4850 2600 4850
Connection ~ 2600 4850
Wire Wire Line
	2600 4850 2950 4850
Connection ~ 2950 4000
Wire Wire Line
	2950 4000 3700 4000
Wire Wire Line
	2200 6200 2300 6200
Wire Wire Line
	2900 6200 2900 5350
Connection ~ 2300 6200
Wire Wire Line
	2300 6200 2500 6200
Connection ~ 2500 6200
Wire Wire Line
	2500 6200 2600 6200
Connection ~ 2600 6200
Wire Wire Line
	2600 6200 2900 6200
Connection ~ 2900 5350
Wire Wire Line
	2900 5350 3700 5350
Wire Wire Line
	4150 4500 4450 4500
Wire Wire Line
	4150 5900 4150 4550
Wire Wire Line
	6450 3950 6450 5300
Wire Wire Line
	6800 5300 6450 5300
Connection ~ 6450 5300
Wire Wire Line
	6450 5300 6450 6500
Wire Wire Line
	6450 2050 6450 2500
Wire Wire Line
	6750 2500 6450 2500
Connection ~ 6450 2500
Wire Wire Line
	6450 2500 6450 3950
Wire Wire Line
	5250 1800 5250 2050
Connection ~ 5250 2050
Wire Wire Line
	5250 2050 6450 2050
Wire Wire Line
	3500 2050 3750 2050
Wire Wire Line
	3750 1800 3750 2050
Connection ~ 3750 2050
Wire Wire Line
	3750 2050 5250 2050
Wire Wire Line
	3500 2050 3500 3200
Wire Wire Line
	2800 3200 3500 3200
Connection ~ 3500 3200
Wire Wire Line
	3500 3200 3500 4650
Wire Wire Line
	2800 4650 3500 4650
Connection ~ 3500 4650
Wire Wire Line
	2800 6000 3500 6000
Wire Wire Line
	3500 4650 3500 6000
$Comp
L power:+3.3V #PWR0103
U 1 1 5E8E8401
P 3150 900
F 0 "#PWR0103" H 3150 750 50  0001 C CNN
F 1 "+3.3V" H 3165 1073 50  0000 C CNN
F 2 "" H 3150 900 50  0001 C CNN
F 3 "" H 3150 900 50  0001 C CNN
	1    3150 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3100 2800 3100
Wire Wire Line
	3150 900  3150 2200
Wire Wire Line
	3150 2200 3850 2200
Wire Wire Line
	6250 2200 6250 2600
Wire Wire Line
	4150 5900 5100 5900
Connection ~ 3150 2200
Wire Wire Line
	3150 2200 3150 3100
Connection ~ 5100 5900
Wire Wire Line
	5100 5900 6250 5900
Wire Wire Line
	2800 5900 4150 5900
Connection ~ 4150 5900
Wire Wire Line
	2800 4550 4150 4550
Connection ~ 4150 4550
Wire Wire Line
	4150 4550 4150 4500
Wire Wire Line
	3850 1800 3850 2200
Connection ~ 3850 2200
Wire Wire Line
	3850 2200 5350 2200
Wire Wire Line
	5350 1800 5350 2200
Connection ~ 5350 2200
Wire Wire Line
	5350 2200 6250 2200
Wire Wire Line
	6750 2600 6250 2600
Connection ~ 6250 2600
Wire Wire Line
	6250 2600 6250 4050
Wire Wire Line
	6800 4050 6250 4050
Connection ~ 6250 4050
Wire Wire Line
	6250 4050 6250 5400
Wire Wire Line
	6800 5400 6250 5400
Connection ~ 6250 5400
Wire Wire Line
	6250 5400 6250 5900
Wire Wire Line
	5100 5900 5100 6650
NoConn ~ 4450 4900
NoConn ~ 4450 5000
NoConn ~ 4450 5100
NoConn ~ 4450 5200
$EndSCHEMATC
